/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
  var a = 10;

  a += 10;
  print(a);

  var i = 0;
  // i++; // i = i + 1;

  var j = i++; // j = 1, i++
  print(i);
  var k = ++i; // k = (++i)

  print(i);
  print(j);
  print(k);
}