/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
  dynamic variable = 100;
  print(variable);

  variable = true;
  print(variable);

  variable = 'Jalal';
  print(variable);
}