/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
  sayHello('Muhamad', 'Jalaludin');
  sayHello('Belajar', 'Dart');
}

void sayHello(String firstName, String lastName) {
  print('Hello $firstName $lastName');
}