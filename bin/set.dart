/// @author Created by Muhamad Jalaludin on 14/04/2022
/// Set sama dengan List tapi set hanya menerima data unique saja.
void main() {
  Set<int> numbers = {};
  var strings = <String>{};
  var doubles = <double>{};

  print(numbers);

  var names = <String>{
    'Muhamad',
    'Jalaludin'
  };

  // names.add('Muhamad');
  // names.add('Muhamad');
  // names.add('Jalaludin');
  // names.add('Jalaludin');

  print(names);
  print(names.length);

  names.remove('Muhamad');
  print(names);
  print(names.length);
}