/// @author Created by Muhamad Jalaludin on 14/04/2022
/// String bisa menggunakan kutip satu ataupun kutip dua, tapi disarankan menggunakan kutip satu.
/// String Interpolation digunakan sebagai expression, dimana di dalam expression kita bisa mengambil data dari variable lain.
/// Karakter \ (backslash) digunakan untuk menekankan bahwa karakter setelahnya dianggap benar karakter tsb.
/// Untuk menggabungkan beberapa string bisa menggunakan karakter + atau karakter whitespace (spasi, enter, tab).
void main() {
  String firstName = 'Muhamad';
  String lastName = "Jalaludin";

  print(firstName);
  print(lastName);

  var fullName = '$firstName ${lastName}';
  print(fullName);

  var text = 'this is \'dart\' \$cool';
  print(text);

  var name1 = firstName + ' ' + lastName;
  var name2 = 'Muhamad' ' Jalaludin';

  print(name1);
  print(name2);

  var longString = '''
this is long string
multiline string
learning dart
  ''';

  print(longString);
}