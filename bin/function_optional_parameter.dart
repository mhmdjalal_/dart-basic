/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
  sayHello('Muhamad', 'Jalaludin');
  sayHello('Dart');
  sayHelloDefault('Muhamad', 'Jalaludin');
  sayHelloDefault('Dart');
}

void sayHello(String firstName, [String? lastName]) {
  print('Hello $firstName $lastName');
}

void sayHelloDefault(String firstName, [String lastName = '']) {
  print('Hello $firstName $lastName');
}