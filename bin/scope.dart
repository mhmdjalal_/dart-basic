/// @author Created by Muhamad Jalaludin on 20/04/2022
void main() {

  var name = 'Jalal';

  void sayHello() {
    var hello = 'Hello $name';
    print(hello);
  }

  sayHello();
  // print(hello); // error
}