/// @author Created by Muhamad Jalaludin on 20/04/2022
void sayHello(String name, String Function(String) filter) {
  print('Hi ${filter(name)}');
}

void main() {
  
  sayHello('Jalal', (name) {
    return name.toUpperCase();
  });

  sayHello('Jalal', (name) => name.toLowerCase());

  var upperFunction = (String name) {
    return name.toUpperCase();
  };

  var lowerFunction = (String name) => name.toLowerCase();

  var res1 = upperFunction('Jalal');
  print(res1);
  var res2 = lowerFunction('Jalal');
  print(res2);
}