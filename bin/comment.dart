/// @author Created by Muhamad Jalaludin on 14/04/2022
/// this is main function
/// will executed by dart
void main() {
  // this is name variable
  var name = 'Muhamad Jalaludin';

  /**
   * this is multiline comment
   * you can add more here
   */
  print(name);
}