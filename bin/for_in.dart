/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {

  var names = <String>['Muhamad', 'Jalaludin'];

  for (var value in names) {
    print(value);
  }

  var namesSet = <String>{'Muhamad', 'Jalaludin'};

  for (var value in namesSet) {
    print(value);
  }
}