/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {

  var nilai = 75;
  var ucapan = nilai >= 75 ? 'Selamat Anda Lulus' : 'Silakan Coba Lagi';

  // if (nilai >= 75) {
  //   ucapan = 'Selamat Anda Lulus';
  // } else {
  //   ucapan = 'Silakan Coba Lagi';
  // }

  print(ucapan);
}