/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
  var data = sayHello('Jalal');
  print(data);

  var total = sum([10, 10, 10, 10, 10]);
  print(total);
}

int sum(List<int> numbers) {
  var total = 0;

  for (var value in numbers) {
    total += value;
  }

  return total;
}

String sayHello(String name) {
  return 'Hello $name';
}