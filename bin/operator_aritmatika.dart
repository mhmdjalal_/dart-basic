/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
  var first = 10;
  var second = 3;
  
  print(first + second);
  print(first - second);
  print(first * second);
  print(first / second);
  print(first ~/ second);
  print(first % second);
}