/// @author Created by Muhamad Jalaludin on 20/04/2022
void main() {

  var counter = 0;

  void increment() {
    print('Increment');
    counter++;
  }

  increment();
  increment();
  print(counter);
}