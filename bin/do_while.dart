/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {

  var counter = 1;

  // minimal satu kali kode dieksekusi
  do {
    print('Perulangan ke-$counter');
    counter++;
  } while (counter <= 10);

}