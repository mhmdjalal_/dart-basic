/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
  Map<String, String> map1 = {};
  var map2 = Map<String, String>();
  var map3 = <String, String>{};

  print(map1);
  print(map2);
  print(map3);

  var name = {
    'first': 'Muhamad',
    'last': 'Jalaludin'
  };
  // var name = <String, String>{};
  // name['first'] = 'Muhamad';
  // name['last'] = 'Jalaludin';

  print(name);
  print(name['first']);

  name['first'] = 'Ahmad';
  print(name['first']);

  name.remove('first');
  print(name);
}