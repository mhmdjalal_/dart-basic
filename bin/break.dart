/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
   var counter = 1;

   while (true) {
      print('Perulangan ke-$counter');
      counter++;

      if (counter > 10) {
         break;
      }
   }
}