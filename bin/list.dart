/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
  List<int> listInt = [];

  var listString = <String>[];

  print(listInt);
  print(listString);

  var names = <String>[
    'Muhamad',
    'Jalaludin'
  ];

  // names.add('Muhamad');
  // names.add('Jalaludin');

  print(names);
  print(names.length);

  print(names[0]);

  names[0] = 'Ahmad';
  print(names[0]);

  names.removeAt(0);
  print(names);
  print(names[0]);
}