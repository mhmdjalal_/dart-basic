/// @author Created by Muhamad Jalaludin on 14/04/2022
/// Best practice penamaan variable menggunakan camel case, seperti variableName.
/// Kata kunci var digunakan sebagai pengganti tipe data.
/// Kata kunci final digunakan jika variable tidak ingin dideklarasikan ulang.
/// Kata kunci const digunakan agar variable dan nilainya menjadi immutable.
/// Kata kunci late digunakan jika kita ingin variable dideklarasikan nanti, ketika varible tersebut diakses saja.
void main() {
  var name = 'Muhamad Jalaludin';
  print(name);
  print(name);
  print(name);
  print(name);

  var firstName = 'Ahmad';
  final lastName = 'Jalaludin';
  firstName = 'Muhamad';
  print(firstName);
  print(lastName);

  final array1 = [1, 2, 3];
  const array2 = [1, 2, 3];

  // array1 = [1, 1, 1]; // tidak bisa deklasari ulang
  array1[0] = 10;

  // array2 = [1, 1, 1]; // tidak bisa deklarasi ulang
  // array2[0] = 10; // tidak bisa ubah nilainya

  print(array1);
  print(array2);

  late var value = getValue();
  print('Variable sudah dibuat');
  print(value);
}

String getValue() {
    print('get value dipanggil');
    return 'Muhamad Jalaludin';
}