/// @author Created by Muhamad Jalaludin on 14/04/2022
/// Kata kunci num digunakan jika kita ingin tipe data number bisa int atau double.
void main() {
  int number1 = 10;
  double number2 = 10.5;

  print(number1);
  print(number2);

  num number = 10;
  print(number);

  number = 10.5;
  print(number);
}