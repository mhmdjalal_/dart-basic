/// @author Created by Muhamad Jalaludin on 14/04/2022
void main() {
  sayHello(firstName: 'Muhamad', lastName: 'Jalaludin');
  sayHello(firstName: 'Dart');
  sayHelloDefault(firstName: 'Muhamad', lastName: 'Jalaludin');
  sayHelloDefault(firstName: 'Dart');
}

void sayHello({String? firstName, String? lastName}) {
  print('Hello $firstName $lastName');
}

/// required parameter
void sayHelloDefault({required String firstName, String lastName = ''}) {
  print('Hello $firstName $lastName');
}